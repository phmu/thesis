All additional data imported from other (Git) projects.

## map-tiles.RData

This data set is just for convenience and contains the map tiles
required to plot the maps of Germany, which will contain circles
indicating the return levels of the individual stations. Occasionally
the GitLab server does not have a proper internet connection and the
rendering of the thesis on server-side fails. To prevent this from
happening, this static set of tiles is provided instead.

It is generated using the following code

``` R
require( OpenStreetMap )
map.tiles <- openmap( upperLeft = c( 56, 5 ),
                     lowerRight = c( 47, 15.5 ),
                     type = "esri-topo" )
map <- openproj( map.tiles, projection = "+proj=longlat" )
save( map, file = "data/map-tiles.RData" )
```

# imports

## [Dwd analysis](https://gitlab.pks.mpg.de/phmu/dwd-analysis)

This project does a large scale analysis of the data set of the DWD
and is responsible for the following files.

- temp-potsdam-distribution.RData
- temp-nonstat-windows.RData
- temp-nonstat-models.RData
- prec-potsdam-distribution.RData
- prec-nonstat-windows.RData
- prec-nonstat-models.RData
- prec.RData

## [ERA-Interim analysis](https://gitlab.pks.mpg.de/phmu/era-interim-analysis)

An even larger scale analysis of the ERA-Interim data set. It
calculates the first four moments, the GEV parameters and return
levels for all grid points as well as some hypotheses test to
determine if a trend in the coefficients is present or not.

It is responsible for the following files:

- era-gev-windows.RData
- era-vglm.RData
