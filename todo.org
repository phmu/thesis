* EVA
** error estimation
   @Yee2007 p.16 claims the error estimation using the MLE can be
   improved by using link functions.

   -> Check

   But on the other hand, I do not want to introduce VGLM just to fit
   the classical univariate EVA.
* non-stationary
** TODO read papers about non-stationary EVA
   - What are they doing?
   - Which approaches do they use?
   - What are their results?
** DONE initial parameter combinations in VGLM/VGAM
   CLOSED: [2018-08-14 Tue 13:51]
   - Does the link functions do the trick?
     -> compare them against raw
     -> are there still artifacts left?

   The link functions do not cover the non-linear constraint but only
   the linear ones. I checked previously if this would work, but it
   didn't.
* analysis
** TODO broken-stick model 
   Yee2007 mentions this piecewise-linear regression spline
   model. This way both the individual trends and their onsets due to
   the stronger change in the mean temperature for the station data
   can be described.
** TODO VGAM fit of time covariate
   Visually display the temporal trends in the GEV parameters for some
   stations. Is the linear model a good one?
** TODO different sites as covariates
   @Yee2007 p. 10 introduced a VGLM model with four different stations
   as covariates for just one single fitted GP distribution

   Does this work? Does it reduce the variance and can be used to make
   more reliable estimates?

   -> problem when wants to use the results for prediction
** impact of the trends in scale and location
   According to @Yee2015 p. 451 the trend in the scale dominates the
   trend in the location parameter.

Zusammenfassung in englisch und deutsch (1-1.5 Seiten) -> in
Prüfungsordnung
- Zusammenfassung der Arbeit mit Ergebnissen vorwegnehmen. Kann ruhig
  Überschneidungen mit Introduction etc haben
