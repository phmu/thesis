This repository contains my thesis. 

## Usage

It is written in a combination of R +
[bookdown](https://bookdown.org/yihui/bookdown/) +
[Pandoc](https://pandoc.org/). The text is written interleaved with
chunks of code, which are delimited using ` ``` ` symbols. The first
time the document is rendered into a specific output format all code
blocks get evaluated, their output, like tables, printed messages, or
plots, captured, and all results cached. In each additional rendering
only code chunks, which were modified after the last run, are
calculated again. 

Firstly, you have to install all required R(>= v3.4) packages by
running the following command in a bash

``` bash
Rscript ./_before_script.R
```

Afterwards, you can render the document into a gitbook using

``` R
bookdown::render_book( "index.Rmd" )
```

in a R shell or into a pdf using

``` R
bookdown::render_book( "index.Rmd", bookdown::pdf_book( template = "custom-template" ) )
```

By moving the lines in the header of [index.Rmd](index.Rmd) concerning
*bookdown::pdf_book:* above the ones of *bookdown::gitbook:*, you can
change the default settings and the first command will render into a
PDF instead.

The resulting files can be found in either *public/index.html* (use
`firefox public/index.html &` in a bash to open and display it) or
*public/_main.pdf*. 

## Structure

All big calculations and simulations are done in different
repositories and referenced in the [simulations.md](simulations.md)
file. Only smaller ones can will be placed in the
[calculations](calculations/README.md) folder.

## General notes

- Always **check your page numbers** in the generated PDF! In my case
  the template was off 1 page-count and the page numbers in the final
  document are printed in the inner and not in the outer parts of the
  page (fixed by now). You just don't notice in `okular`...
- [Here](https://tu-dresden.de/mn/postgraduales/promotion?set_language=de)
  you can find a list of all requirements in order to obtain the
  PhD. But it's not entirely valid. *You do not need four printed
  versions, unless you have three reviewers*. (Which will most
  probably be the case). **You do not have to provide a CD/DVD.** In
  fact, they won't take it even if you did.
- You **must** use
  [Promovendus](https://promovendus.tu-dresden.de/home) to start the
  Promotionsverfahren. There is not other way. But you do not have to
  enter all the stuff you did during your PhD, like conferences and
  lectures you attended.
- The certificate ?and certificate? (we have to provide both the
  "Urkunde" and "Zeugnis") of the Bachelor and Master study have to be
  **"amtlich beglaubigt"**. This means you have to make a copy of the
  original yourself, stand in line in the
  [Bürgeramt](http://www.dresden.de/de/rathaus/aemter-und-einrichtungen/oe/dborg/stadt_dresden_5976.php)
  (you have to bring both the copy and the original), and have to pay
  a person to put a stamp on the copy. You also need a copy of your
  Highschool degree, but it does not have to be "beglaubigt".
- You also have to hand in a **beglaubigte** copy of all grades you
  received during your Bachelor and Master.
- You do not need the [erklaerung.md](erklaerung.md). I introduced it
  prior to the me knowing about the Promovendus page.
