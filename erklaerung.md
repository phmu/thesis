# Erklärung

Hiermit versichere ich, dass ich die vorliegende Arbeit zum Thema **Extreme value analysis of non-stationary time series - Quantifying climate change using observational data throughout Germany** ohne unzulässige Hilfe Dritter und ohne Benutzung anderer als der angegebenen Hilfsmittel angefertigt habe; die aus fremden Quellen direkt oder indirekt übernommenen Gedanken sind als solche kenntlich gemacht. Die Arbeit wurde bisher weder im Inland noch im Ausland in gleicher oder ähnlicher Form einer anderen Prüfungsbehörde vorgelegt.

Die Arbeit wurde am Max-Planck-Institut für Physik komplexer Systeme in der Abteilung *Nichtlineare Dynamik und Zeitreihenanalyse* angefertigt und von Prof. Dr. Holger Kantz betreut. Es hat kein früheres erfolgloses Promotionsverfahren stattgefunden. Ich erkenne die Promotionsordnung der Fakultät Mathematik und Naturwissenschaften der Technischen Universität Dresden vom 23.02.2011 in ihrer letzten Änderung vom 18.07.2018 an.

Hiermit bestätige ich außerdem, dass das überreichte Führungszeugnis nicht älter als drei Monate ist.

# Vorschläge Gutachter:

1. Prof. Dr. Holger Kantz (TUD/MPIPKS Dresden) (kantz@pks.mpg.de)
2. Prof. Dr. Jürgen Kurths (Potsdam Institute for Climate Impact Research - PIK) (Juergen.Kurths@pik-potsdam.de)
3. Prof. Dr. Marc Timme (TUD) (marc.timme@tu-dresden.de) (intern)

# Vorschläge Rigorosum:

1. **Stochastische Prozesse** bei Prof. Dr. Holger Kantz (kantz@pks.mpg.de)
2. **Statistical Methods of Data Analysis** bei Prof. Dr. Arno Straessner (Arno.Straessner@tu-dresden.de)

&nbsp;

&nbsp;

&nbsp;

Dresden, 18.10.2018

---
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (Philipp Müller)
