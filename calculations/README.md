This folder contains smaller calculations, which are sufficiently fast
and small to not need a separate treatment. All results will be stored
in the local [data](calculations/data) folder.

## [constrained-optimization](calculations/constrained-optimization.R)

Checking for the dependence of the results of the constrained
optimization on the initial parameter combination.

## [dwd-stations](calculations/dwd-stations.R)

Script calculating the shape and return levels of the DWD data
including their standard deviation estimated using various approaches.

## [error-estimation-monte-carlo](calculations/error-estimation-monte-carlo.R)

Determining the differences between the estimation of the fitting
error using the Monte Carlo methods and the delta method, which is
based on the maximum likelihood estimates.

## [failed-likelihood](calculations/failed-likelihood.R)

Underlining the difference of the default likelihood and the augmented
Lagrangian by plotting intersections of the likelihood space for
several points.

## [skewness-estimator](calculations/skewness-estimator.R)

Used to produce **fig:skewness**.

Contains two data frames, which can be plotted using the **ggplot2**
package.

The skewness of sampled GEV/GP time series was calculated for given
parameter combinations. The overall goal of this data is to give
insight in the skewness dependence of the shape parameter in order to
produce a more accurate fitting heuristic.
